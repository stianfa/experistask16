﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Task15
{
    public class Customers : IEnumerable
    {
        private Customer[] customers;

        public Customers(Customer[] pArray)
        {
            customers = new Customer[pArray.Length];

            for (int i = 0; i < pArray.Length; i++)
            {
                customers[i] = pArray[i];
            }
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public CustomersEnum GetEnumerator()
        {
            return new CustomersEnum(customers);
        }
    }
}
