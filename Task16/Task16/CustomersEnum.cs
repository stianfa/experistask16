﻿using System;
using System.Collections;
using System.Linq;

namespace Task15
{
    public class CustomersEnum : IEnumerator
    {
        public Customer[] customers;

        int position = -1;

        public CustomersEnum(Customer[] list)
        {
            customers = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < customers.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Customer Current
        {
            get
            {
                try
                {
                    return customers[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}