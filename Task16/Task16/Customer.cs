﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Task15
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public double AmountOwed { get; set; }

        public Customer(int customerID, string name, double amountOwed)
        {
            CustomerID = customerID;
            Name = name;
            AmountOwed = amountOwed;
        }
    }
}
