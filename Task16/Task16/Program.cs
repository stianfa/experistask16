﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task15
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer[] customers = new Customer[3]
       {
            new Customer(1, "John", 101.0),
            new Customer(2, "Peter", 150.0),
            new Customer(3, "Jessica", 90.0),
       };


            // Collection foreach works:
            Customers customersObj = new Customers(customers);
            foreach (Customer c in customers)
                Console.WriteLine($"ID: {c.CustomerID}. Name: {c.Name}. Amount owed: {c.AmountOwed}");



            // Linq queries on array
            var highDebtCustomers = from c in customers
                                    where c.AmountOwed > 100
                                    select c;

            Console.WriteLine("The following customers has a high debt: ");
            foreach (var c in highDebtCustomers)
            {
                Console.WriteLine(c.Name);
            }

            var longNameCustomers = from cus in customers
                                    where cus.Name.Length > 5
                                    select cus;

            Console.WriteLine("The following customers has a long name: ");
            foreach (var c in longNameCustomers)
            {
                Console.WriteLine(c.Name);
            }
        }
    }
}
